
package science.unlicense.project.uieditor;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.OrderedHashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.DefaultProperty;
import science.unlicense.common.api.event.EventSource;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.lang.Reflection;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.Writer;
import science.unlicense.engine.ui.io.RSWriter;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.format.json.JSONUtilities;
import science.unlicense.format.json.JSONWriter;
import science.unlicense.project.uieditor.adaptor.ValueAdaptor;

/**
 *
 * @author Johann Sorel
 */
public class JSWidgetWriter implements Writer {

    private final JSONWriter writer;

    public JSWidgetWriter() {
        this.writer = new JSONWriter();
        writer.setFormatted(true);
        writer.setEncoding(CharEncodings.UTF_8);
        writer.setIndentation(new Chars("  "));
    }

    @Override
    public void setOutput(Object output) throws IOException {
        writer.setOutput(output);
    }

    @Override
    public Object getOutput() {
        return writer.getOutput();
    }

    @Override
    public void dispose() throws IOException {
        writer.dispose();
    }

    public void write(Widget widget) throws IOException{
        final Dictionary node;
        try {
            node = toDictionary(widget);
        } catch (InstantiationException ex) {
            throw new IOException(ex.getMessage(), ex);
        } catch (IllegalAccessException ex) {
            throw new IOException(ex.getMessage(), ex);
        }
        JSONUtilities.writeDictionary(node, writer);
    }


    /**
     * Convert widget to a dictionary.
     */
    private Dictionary toDictionary(EventSource candidate) throws InstantiationException, IllegalAccessException {

        final Dictionary dico = new OrderedHashDictionary();

        //class informations
        dico.add(UIConstants.ATT_CLASS, new Chars(candidate.getClass().getName()));

        //create a default reference object for default values
        final EventSource ref = candidate.getClass().newInstance();

        final Sequence propertyNames = Reflection.listProperties(candidate.getClass());

        final boolean isContainer = candidate.getClass().equals(WContainer.class);

        for(int i=0,n=propertyNames.getSize();i<n;i++){
            final Chars propertyName = (Chars)propertyNames.get(i);
            final Property p = new DefaultProperty(candidate, propertyName);
            Object obj = p.getValue();
            if (obj == null) continue;

            //compare with default value
            final Property pref = new DefaultProperty(ref, propertyName);
            final Object objref = pref.getValue();
            if(CObjects.equals(obj,objref)) continue;


            if(!(obj instanceof CharArray)){
                final ValueAdaptor adaptor = UIConstants.getAdaptor(obj.getClass());
                if(adaptor!=null){
                    obj = adaptor.convert(obj);
                }
            }

            if(obj instanceof CharArray){
                dico.add(propertyName, (CharArray)obj);
            }else if(obj instanceof Dictionary){
                dico.add(propertyName, (Dictionary)obj);
            }else if(WContainer.PROPERTY_LAYOUT.equals(propertyName) && isContainer){
                dico.add(propertyName, toDictionary((EventSource) obj));
            }

        }

        if (isContainer) {
            final Widget widget = (Widget) candidate;
            final Object[] children = widget.getChildren().toArray();
            if(children.length>0){
                final Dictionary[] childNodes = new Dictionary[children.length];
                for(int i=0;i<children.length;i++){
                    childNodes[i] = toDictionary((Widget) children[i]);
                }
                dico.add(UIConstants.ATT_CHILDREN, childNodes);
            }
        }

        if (candidate instanceof Widget) {
            final Widget widget = (Widget) candidate;

            final Chars rules = RSWriter.rulesToChars(widget.getStyle());
            if (!rules.isEmpty()) {
                dico.add(UIConstants.ATT_STYLE, rules);
            }

            final Chars selfStyle = RSWriter.propertiesToChars(widget.getStyle().getSelfRule());
            if (!selfStyle.isEmpty()) {
                dico.add(UIConstants.ATT_SELFSTYLE, selfStyle);
            }
        }

        return dico;
    }

    public static Chars encode(Widget widget) throws IOException {
        final ArrayOutputStream out = new ArrayOutputStream();

        final JSWidgetWriter writer = new JSWidgetWriter();
        writer.setOutput(out);
        writer.write(widget);

        return new Chars(out.getBuffer().toArrayByte(),CharEncodings.UTF_8);
    }

    @Override
    public void setConfiguration(Document configuration) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Document getConfiguration() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
