
package science.unlicense.project.uieditor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.engine.ui.component.WButtonBar;
import science.unlicense.engine.ui.component.WCrumbBar;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WCheckBox;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicGeometry;
import science.unlicense.engine.ui.widget.WGraphicText;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WProgressBar;
import science.unlicense.engine.ui.widget.WScrollBar;
import science.unlicense.engine.ui.widget.WSelect;
import science.unlicense.engine.ui.widget.WSeparator;
import science.unlicense.engine.ui.widget.WSlider;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.ui.widget.WSplitContainer;
import science.unlicense.engine.ui.widget.WSwitch;
import science.unlicense.engine.ui.widget.WTabContainer;
import science.unlicense.engine.ui.widget.WTable;
import science.unlicense.engine.ui.widget.WTextArea;
import science.unlicense.engine.ui.widget.WTextField;
import science.unlicense.engine.ui.widget.WTree;
import science.unlicense.engine.ui.widget.WTreeTable;
import science.unlicense.engine.ui.widget.chart.WChart;
import science.unlicense.engine.ui.widget.frame.WDesktop;
import science.unlicense.engine.ui.widget.menu.WMenuButton;
import science.unlicense.engine.ui.widget.menu.WMenuSwitch;
import science.unlicense.project.uieditor.adaptor.BooleanAdaptor;
import science.unlicense.project.uieditor.adaptor.DoubleAdaptor;
import science.unlicense.project.uieditor.adaptor.FloatAdaptor;
import science.unlicense.project.uieditor.adaptor.IntegerAdaptor;
import science.unlicense.project.uieditor.adaptor.LayoutContraintAdaptor;
import science.unlicense.project.uieditor.adaptor.ValueAdaptor;

/**
 *
 * @author Johann Sorel
 */
public class UIConstants {

    public static final Chars ATT_CLASS = new Chars("@class");
    public static final Chars ATT_CONSTRUCTOR = new Chars("@constructor");
    public static final Chars ATT_SELFSTYLE = new Chars("@selfrule");
    public static final Chars ATT_STYLE = new Chars("@style");
    public static final Chars ATT_CHILDREN = new Chars("children");

    static final ValueAdaptor[] ADAPTORS = new ValueAdaptor[]{
        new BooleanAdaptor(),
        new IntegerAdaptor(),
        new FloatAdaptor(),
        new DoubleAdaptor(),
        new LayoutContraintAdaptor()
    };

    public static final Sequence WIDGET_CLASSES = new ArraySequence();
    static {
        WIDGET_CLASSES.add(WButton.class);
        WIDGET_CLASSES.add(WCheckBox.class);
        WIDGET_CLASSES.add(WContainer.class);
        WIDGET_CLASSES.add(WGraphicGeometry.class);
        WIDGET_CLASSES.add(WGraphicText.class);
        WIDGET_CLASSES.add(WLabel.class);
        WIDGET_CLASSES.add(WProgressBar.class);
        WIDGET_CLASSES.add(WScrollBar.class);
        WIDGET_CLASSES.add(WSelect.class);
        WIDGET_CLASSES.add(WSeparator.class);
        WIDGET_CLASSES.add(WSlider.class);
        WIDGET_CLASSES.add(WSpace.class);
        WIDGET_CLASSES.add(WSpinner.class);
        WIDGET_CLASSES.add(WSpinner.class);
        WIDGET_CLASSES.add(WSplitContainer.class);
        WIDGET_CLASSES.add(WSwitch.class);
        WIDGET_CLASSES.add(WTabContainer.class);
        WIDGET_CLASSES.add(WTable.class);
        WIDGET_CLASSES.add(WTextArea.class);
        WIDGET_CLASSES.add(WTextField.class);
        WIDGET_CLASSES.add(WTree.class);
        WIDGET_CLASSES.add(WTreeTable.class);

        WIDGET_CLASSES.add(WMenuButton.class);
        WIDGET_CLASSES.add(WMenuSwitch.class);

        WIDGET_CLASSES.add(WDesktop.class);

        WIDGET_CLASSES.add(WChart.class);

        WIDGET_CLASSES.add(WButtonBar.class);
        WIDGET_CLASSES.add(WCrumbBar.class);
    }


    public static ValueAdaptor getAdaptor(Class input){
        for(ValueAdaptor c : ADAPTORS){
            if(c.getInputClass().isAssignableFrom(input)){
                return c;
            }
        }
        return null;
    }

}
