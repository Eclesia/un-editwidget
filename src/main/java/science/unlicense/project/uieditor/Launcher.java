
package science.unlicense.project.uieditor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.desktop.Frame;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.engine.swing.SwingFrameManager;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.io.RSReader;
import science.unlicense.engine.ui.style.SystemStyle;
import science.unlicense.engine.ui.style.WStyle;
import science.unlicense.engine.ui.style.WidgetStyles;


/**
 *
 * @author Johann Sorel
 */
public class Launcher {

    public static void main(String[] args) {

        WStyle style = RSReader.readStyle(Paths.resolve(SystemStyle.THEME_DARK));
        WidgetStyles.mergeDoc(
                SystemStyle.INSTANCE.getRule(SystemStyle.RULE_SYSTEM),
                style.getRule(SystemStyle.RULE_SYSTEM), false);
        SystemStyle.INSTANCE.notifyChanged();


        final WUIEditor editor = new WUIEditor();

        final UIFrame frame = SwingFrameManager.INSTANCE.createFrame(false);//FrameManagers.getFrameManager().createFrame(false);
        frame.addEventListener(new PropertyPredicate(Frame.PROP_VISIBLE), new EventListener() {
            public void receiveEvent(Event event) {
                if(((PropertyMessage)event.getMessage()).getNewValue() == Boolean.FALSE){
                    System.exit(0);
                }
            }
        });
        frame.getContainer().setLayout(new BorderLayout());
        frame.getContainer().addChild(editor, BorderConstraint.CENTER);

        frame.setTitle(new Chars("Un-WidgetEditor"));
        frame.setSize(1024, 768);
        frame.setVisible(true);

    }

}
