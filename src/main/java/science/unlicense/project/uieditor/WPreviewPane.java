
package science.unlicense.project.uieditor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.common.api.event.Property;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.AbsoluteLayout;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.display.api.layout.Layout;
import science.unlicense.display.api.layout.StackConstraint;
import science.unlicense.display.api.layout.StackLayout;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.engine.ui.widget.Widgets;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.geometry.impl.TransformedGeometry;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.impl.Affine2;
import static science.unlicense.project.uieditor.WStyleSelfRuleTable.PROPERTY_EDITED;

/**
 *
 * @author Johann Sorel
 */
public class WPreviewPane extends WContainer {

    public static final Chars PROPERTY_ROOTWIDGET = new Chars("RootWidget");

    private final WSelectionPaint halo = new WSelectionPaint();
    private final WContainer fakePane = new WContainer(new BorderLayout()) {
        @Override
        protected void receiveEventParent(Event event) {
            if(event.getMessage() instanceof MouseMessage || event.getMessage() instanceof KeyMessage){
                //we disable preview widgets behavior
                return;
            }
            super.receiveEventParent(event);
        }
    };

    public WPreviewPane() {
        super(new StackLayout());

        getStyle().getSelfRule().setProperties(new Chars("background:{\nfill-paint:@color-main\n}"));
        final WContainer centered = new WContainer(new AbsoluteLayout());
        fakePane.getStyle().getSelfRule().setProperties(new Chars("background:{\nfill-paint:@color-background\n}"));
        centered.addChild(fakePane, null);

        addChild(centered, new StackConstraint(0));
        addChild(halo, new StackConstraint(1));
    }

    public WContainer getRootWidget() {
        return (WContainer) getPropertyValue(PROPERTY_ROOTWIDGET);
    }

    public void setRootWidget(WContainer root) {
        if (setPropertyValue(PROPERTY_ROOTWIDGET, root)) {
            fakePane.getChildren().removeAll();
            if (root != null) {
                fakePane.addChild(root,BorderConstraint.CENTER);
            }
            halo.setDirty();
        }
    }

    public Property varRootWidget() {
        return getProperty(PROPERTY_ROOTWIDGET);
    }

    public Object getEdited(){
        return (Object) getPropertyValue(PROPERTY_EDITED);
    }

    public void setEdited(Object root){
        if(setPropertyValue(PROPERTY_EDITED, root)){
            halo.setDirty();
        }
    }

    public Property varEdited(){
        return getProperty(PROPERTY_EDITED);
    }

    private final class WSelectionPaint extends WContainer {

        public WSelectionPaint() {
            setView(new WidgetView(this) {
                @Override
                protected void renderSelf(Painter2D painter, BBox innerBBox) {
                    super.renderSelf(painter, innerBBox);

                    final Object bean = getEdited();
                    if(bean instanceof Widget){
                        final Widget widget = (Widget) bean;
                        final Affine2 rootToNodeSpace = getRootToNodeSpace(widget);
                        final BBox bbox = widget.getBoundingBox();

                        painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_BUTT));
                        painter.setPaint(new ColorPaint(Color.BLUE));
                        painter.stroke(TransformedGeometry.create(new Rectangle(bbox), rootToNodeSpace.invert()));

                        if(bean instanceof WContainer){
                            final Layout layout = ((WContainer) bean).getLayout();
                            if(layout instanceof FormLayout){
                                final FormLayout fl = (FormLayout) layout;
                            }
                        }

                    }
                }
            });
        }

        @Override
        protected void receiveEventParent(Event event) {
            super.receiveEventParent(event);

            final EventMessage message = event.getMessage();
            if (message instanceof MouseMessage) {
                final MouseMessage me = (MouseMessage) message;
                if (me.getType() == MouseMessage.TYPE_TYPED) {
                    final Sequence stack = Widgets.pruneStackAt(fakePane, me.getMousePosition().get(0), me.getMousePosition().get(1));
                    Widget valid = null;
                    for (int i = 0, n = stack.getSize(); i < n; i++) {
                        final Widget candidate = (Widget) stack.get(i);
                        valid = candidate;
                        if (UIConstants.WIDGET_CLASSES.contains(candidate.getClass())) {
                            valid = candidate;
                            if (valid.getClass() != WContainer.class) {
                                break;
                            }
                        }
                    }
//                    for(int i=stack.getSize()-1;i>=0;i--){
//                        final Widget candidate = (Widget) stack.get(i);
//                        valid = candidate;
//                        if(UIConstants.WIDGET_CLASSES.contains(candidate.getClass())){
//                            valid = candidate;
//                            break;
//                        }
//                    }
                    if (valid != null && valid != fakePane) {
                        setEdited(valid);
                    }
                }
            }

        }

        public synchronized Affine2 getRootToNodeSpace(SceneNode widget) {
            final Affine2 rootToNode = new Affine2();
            if (widget == WPreviewPane.this) {
                return rootToNode;
            }
            rootToNode.set(widget.getNodeTransform().invert());
            SceneNode parent = widget.getParent();
            if (parent != null) {
                final Affine2 pm = getRootToNodeSpace(parent);
                rootToNode.localMultiply(pm);
            }
            return rootToNode;
        }

    }
}