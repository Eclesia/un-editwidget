
package science.unlicense.project.uieditor.bean;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.display.api.layout.Layout;
import science.unlicense.engine.ui.component.bean.PropertyEditor;
import science.unlicense.engine.ui.model.ArrayLabelEditor;
import science.unlicense.engine.ui.model.ArraySpinnerModel;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.ui.widget.WValueWidget;
import science.unlicense.engine.ui.widget.Widget;


/**
 *
 * @author Johann Sorel
 */
public class LayoutConstraintEditor implements PropertyEditor {

    public static final LayoutConstraintEditor INSTANCE = new LayoutConstraintEditor();

    private LayoutConstraintEditor(){}

    @Override
    public WValueWidget create(Property property) {
        if (!WContainer.PROPERTY_LAYOUT_CONSTRAINT.equals(property.getName())) {
            return null;
        }

        final Widget w = (Widget) property.getHolder();
        final WContainer parent = (WContainer) w.getParent();
        if (parent == null) return null;

        final Layout layout = parent.getLayout();
        if (layout instanceof BorderLayout) {
            final BorderConstraintEditor cst = new BorderConstraintEditor();
            cst.varValue().sync(property);
            return cst;
        } else if(layout instanceof FormLayout) {
            return new FormLayoutEditor(property);
        } else {
            return null;
        }
    }

    private static class BorderConstraintEditor extends WSpinner {

        public BorderConstraintEditor() {
            super(new ArraySpinnerModel(new Object[]{
                BorderConstraint.CENTER,
                BorderConstraint.TOP,
                BorderConstraint.RIGHT,
                BorderConstraint.BOTTOM,
                BorderConstraint.LEFT}));
            setEditor(new ArrayLabelEditor(){
                public Chars toText(Object value) {
                    if(BorderConstraint.CENTER.equals(value)) {
                        return new Chars("CENTER");
                    } else if(BorderConstraint.TOP.equals(value)) {
                        return new Chars("TOP");
                    } else if(BorderConstraint.RIGHT.equals(value)) {
                        return new Chars("RIGHT");
                    } else if(BorderConstraint.BOTTOM.equals(value)) {
                        return new Chars("BOTTOM");
                    } else if(BorderConstraint.LEFT.equals(value)) {
                        return new Chars("LEFT");
                    }
                    return super.toText(value);
                }
            });
        }
    }

    private static class FormLayoutEditor extends WContainer implements WValueWidget {

        private WSpinner x = new WSpinner(new NumberSpinnerModel(Integer.class, 0, 0, Integer.MAX_VALUE, 1));
        private WSpinner y = new WSpinner(new NumberSpinnerModel(Integer.class, 0, 0, Integer.MAX_VALUE, 1));
        private WSpinner spanx = new WSpinner(new NumberSpinnerModel(Integer.class, 1, 1, Integer.MAX_VALUE, 1));
        private WSpinner spany = new WSpinner(new NumberSpinnerModel(Integer.class, 1, 1, Integer.MAX_VALUE, 1));
        private final Property property;
        private boolean updating = false;

        public FormLayoutEditor(Property property) {
            super(new FormLayout(1, 1));
            this.property = property;
            addChild(new WLabel(new Chars("X")), FillConstraint.builder().coord(0, 0).build());
            addChild(x, FillConstraint.builder().coord(1, 0).build());
            addChild(new WLabel(new Chars("Y")), FillConstraint.builder().coord(2, 0).build());
            addChild(y, FillConstraint.builder().coord(3, 0).build());
            addChild(new WLabel(new Chars("SpX")), FillConstraint.builder().coord(4, 0).build());
            addChild(spanx, FillConstraint.builder().coord(5, 0).build());
            addChild(new WLabel(new Chars("SpY")), FillConstraint.builder().coord(6, 0).build());
            addChild(spany, FillConstraint.builder().coord(7, 0).build());

            final EventListener listener = new EventListener(){
                public void receiveEvent(Event event) {
                    if(updating) return;
                    updateProperty();
                }
            };

            x.addEventListener(new PropertyPredicate(WSpinner.PROPERTY_VALUE), listener);
            y.addEventListener(new PropertyPredicate(WSpinner.PROPERTY_VALUE), listener);
            spanx.addEventListener(new PropertyPredicate(WSpinner.PROPERTY_VALUE), listener);
            spany.addEventListener(new PropertyPredicate(WSpinner.PROPERTY_VALUE), listener);

            property.addEventListener(PropertyMessage.PREDICATE, new EventListener() {
                public void receiveEvent(Event event) {
                    updateFields();
                }
            });

            updateFields();
        }

        @Override
        public Chars getValuePropertyName() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        private void updateProperty(){
            final FillConstraint cst = FillConstraint.builder()
                    .coord((Integer)x.getValue(),(Integer)y.getValue())
                    .span((Integer)spanx.getValue(),(Integer)spany.getValue())
                    .build();
            property.setValue(cst);
        }

        private void updateFields(){
            updating=true;
            Object cst = property.getValue();
            if (cst instanceof FillConstraint) {
                final FillConstraint fcst = (FillConstraint) cst;
                x.setValue(fcst.getX());
                y.setValue(fcst.getY());
                spanx.setValue(fcst.getSpanX());
                spany.setValue(fcst.getSpanY());
            } else {
                //update the constraint
                x.setValue(0);
                y.setValue(0);
                spanx.setValue(1);
                spany.setValue(1);
                updateProperty();
            }
            updating = false;
        }

    }

}
