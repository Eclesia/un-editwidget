
package science.unlicense.project.uieditor.bean;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.predicate.AbstractVariable;
import science.unlicense.display.api.layout.AbsoluteLayout;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.CircularLayout;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.display.api.layout.Layout;
import science.unlicense.display.api.layout.LineLayout;
import science.unlicense.engine.ui.component.bean.PropertyEditor;
import science.unlicense.engine.ui.model.ArrayLabelEditor;
import science.unlicense.engine.ui.model.ArraySpinnerModel;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.ui.widget.WValueWidget;


/**
 *
 * @author Johann Sorel
 */
public class LayoutEditor implements PropertyEditor {

    public static final LayoutEditor INSTANCE = new LayoutEditor();

    private LayoutEditor(){}

    @Override
    public WValueWidget create(Property property) {
        if(!(WContainer.PROPERTY_LAYOUT.equals(property.getName()) && property.getHolder().getClass().equals(WContainer.class))){
            return null;
        }

        final WContainer w = (WContainer) property.getHolder();
        final Editor editor = new Editor(w);
        return editor;
    }

    private static class LayoutVar extends AbstractVariable {

        private final WContainer container;

        public LayoutVar(WContainer container) {
            this.container = container;
        }

        @Override
        public Object getValue() throws RuntimeException {
            return container.getLayout().getClass();
        }

        @Override
        public void setValue(Object value) throws RuntimeException {
            try {
                final Layout old = container.getLayout();
                final Object layout = ((Class)value).newInstance();
                container.setLayout((Layout) layout);
                fireValueChanged(old.getClass(), value);
            } catch (InstantiationException ex) {
                ex.printStackTrace();
            } catch (IllegalAccessException ex) {
                ex.printStackTrace();
            }
        }
    }

    private static class Editor extends WSpinner {

        private final LayoutVar var;

        public Editor(WContainer w) {
            super(new ArraySpinnerModel(new Object[]{
                AbsoluteLayout.class,
                BorderLayout.class,
                FormLayout.class,
                GridLayout.class,
                LineLayout.class,
                CircularLayout.class}));
            setEditor(new ArrayLabelEditor(){
                @Override
                public Chars toText(Object value) {
                    if(value instanceof Class){
                        return new Chars(((Class)value).getSimpleName());
                    }
                    return super.toText(value);
                }
            });
            var = new LayoutVar(w);//keep reference, sync uses a weak reference
            varValue().sync(var);
        }

    }

}
