
package science.unlicense.project.uieditor;

import java.lang.reflect.Field;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.event.DefaultProperty;
import science.unlicense.common.api.event.EventSource;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.lang.Reflection;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.format.json.JSONUtilities;
import science.unlicense.project.uieditor.adaptor.ValueAdaptor;

/**
 *
 * @author Johann Sorel
 */
public class JSWidgetReader extends AbstractReader{

    private JSWidgetReader(){}

    public static Widget decode(Object input, Widget root) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        if(input instanceof CharArray){
            input = new ArrayInputStream(((CharArray)input).toBytes(CharEncodings.UTF_8));
        }
        final JSWidgetReader reader = new JSWidgetReader();
        reader.setInput(input);
        return reader.read(root);
    }

    private Widget read(Widget parent) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        final Dictionary dico = JSONUtilities.readAsDictionary(getInput());

        if (parent == null) {
            parent = (Widget) create(dico);
        }
        fill(parent, dico);

        return parent;
    }

    private Object create(Dictionary dico) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        final Chars className = (Chars) dico.getValue(UIConstants.ATT_CLASS);
        final Chars constructor = (Chars) dico.getValue(UIConstants.ATT_CONSTRUCTOR);
        return (Object) Class.forName(className.toString()).newInstance();
    }

    private void fill(Object candidate, Dictionary dico) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException{

        final Iterator ite = dico.getKeys().createIterator();
        while (ite.hasNext()) {
            final Chars key = (Chars) ite.next();
            Object value = dico.getValue(key);

            if (UIConstants.ATT_STYLE.equals(key)) {
                ((Widget) candidate).getStyle().addRules((Chars) value);
            } else if (UIConstants.ATT_SELFSTYLE.equals(key)) {
                ((Widget) candidate).getStyle().getSelfRule().setProperties((Chars) value);
            }
            if (key.startsWith('@')) {
                //configuration parameters skip it
                continue;
            } else if (UIConstants.ATT_CHILDREN.equals(key)) {
                final Object[] values = (Object[]) value;
                for (Object childObj : values) {
                    final Dictionary childDico = (Dictionary) childObj;
                    final Object child = create(childDico);
                    fill(child, childDico);
                    ((WContainer) candidate).getChildren().add(child);
                }
            } else {

                if (value instanceof Dictionary) {
                    //value is an object
                    Dictionary subDico = (Dictionary) value;
                    value = create(subDico);
                    fill(value, subDico);
                }


                if (candidate instanceof EventSource) {
                    //adapt value to property type
                    final Property property = new DefaultProperty((EventSource) candidate, key);
                    ValueAdaptor adaptor = UIConstants.getAdaptor(property.getType().getValueClass());
                    if (adaptor != null) {
                        value = adaptor.reverse(value);
                    }
                    if (value == null) {
                        throw new IllegalArgumentException("Null value for property "+key);
                    } else if (property.getType().getValueClass().isAssignableFrom(value.getClass())) {
                        property.setValue(value);
                    }
                } else {
                    //search for private properties
                    final Field[] declaredFields = candidate.getClass().getDeclaredFields();
                    for (Field f : declaredFields){
                        if (f.getName().equals(key.toString())) {
                            f.setAccessible(true);
                            final Class typeClass = Reflection.getBoxingClass(f.getType());
                            if (!typeClass.isAssignableFrom(value.getClass())) {
                                ValueAdaptor adaptor = UIConstants.getAdaptor(typeClass);
                                if (adaptor != null) {
                                    value = adaptor.reverse(value);
                                }
                            }
                            if (value == null) {
                                throw new IllegalArgumentException("Null value for property "+key);
                            } else if (typeClass.isAssignableFrom(value.getClass())) {
                                f.set(candidate, value);
                            }
                            break;
                        }
                    }
                }
            }

        }

    }

}
