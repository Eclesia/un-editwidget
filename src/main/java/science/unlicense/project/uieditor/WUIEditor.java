package science.unlicense.project.uieditor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.CollectionMessage;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventSource;
import science.unlicense.common.api.event.MessagePredicate;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.common.api.model.tree.DefaultNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.desktop.DragAndDropMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.display.api.layout.Layout;
import science.unlicense.display.api.layout.SplitConstraint;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.component.WButtonBar;
import science.unlicense.engine.ui.component.bean.WBeanTable;
import science.unlicense.engine.ui.component.path.WPathChooser;
import science.unlicense.engine.ui.io.RSReader;
import science.unlicense.engine.ui.model.DefaultColumn;
import science.unlicense.engine.ui.model.DefaultRowModel;
import science.unlicense.engine.ui.model.DefaultTreeColumn;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.model.TreeRowModel;
import science.unlicense.engine.ui.style.SystemStyle;
import science.unlicense.engine.ui.style.WStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicText;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSeparator;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.WSplitContainer;
import science.unlicense.engine.ui.widget.WTabContainer;
import science.unlicense.engine.ui.widget.WTable;
import science.unlicense.engine.ui.widget.WTreeTable;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.engine.ui.widget.menu.WMenuButton;
import science.unlicense.system.api.desktop.Attachment;
import science.unlicense.system.api.desktop.DefaultAttachment;
import science.unlicense.project.uieditor.bean.LayoutConstraintEditor;
import science.unlicense.project.uieditor.bean.LayoutEditor;

/**
 *
 * @author Johann Sorel
 */
public class WUIEditor extends WContainer {

    public static final Chars PROPERTY_EDITED = new Chars("Edited");
    public static final Chars PROPERTY_ROOTWIDGET = new Chars("RootWidget");
    private static final Chars FONT_STYLE = new Chars("font:{\nfamily:font('Unicon' 14 'none')\n}");

    private final WButtonBar toolbar = new WButtonBar();
    private final WTable paletteTable = new WTable();
    private final WTreeTable widgetTree = new WTreeTable();
    private final WStyleSelfRuleTable styleSelfRuleTable = new WStyleSelfRuleTable();
    private final WStyleTable styleTable = new WStyleTable();
    private final WPreviewPane previewPane = new WPreviewPane();
    private final WTabContainer tabs = new WTabContainer();
    private final WBeanTable beanTable = new WBeanTable();

    private final EventListener openListener = new EventListener() {
        public void receiveEvent(Event event) {
            new Thread(){
                public void run() {
                    final Path path = WPathChooser.showOpenDialog(Predicate.TRUE);
                    if(path!=null){
                        try{
                            final WContainer cst = (WContainer) JSWidgetReader.decode(path,null);
                            setRootWidget(cst);
                        }catch(Exception ex){
                            ex.printStackTrace();
                        }
                    }
                }
            }.start();
        }
    };

    private final EventListener saveListener = new EventListener() {
        public void receiveEvent(Event event) {
            new Thread(){
                public void run() {
                    final Path path = WPathChooser.showSaveDialog(Predicate.TRUE);
                    if(path!=null){
                        try{
                            final JSWidgetWriter writer = new JSWidgetWriter();
                            writer.setOutput(path);
                            writer.write(getRootWidget());
                            writer.dispose();
                        }catch(Exception ex){
                            ex.printStackTrace();
                        }
                    }
                }
            }.start();
        }
    };

    public WUIEditor(){
        super(new BorderLayout());

        beanTable.getEditors().add(LayoutConstraintEditor.INSTANCE);
        beanTable.getEditors().add(LayoutEditor.INSTANCE);

        tabs.setTabPosition(WTabContainer.TAB_POSITION_LEFT);
        final WGraphicText attLbl = new WGraphicText(new Chars("\uE026"));
        attLbl.getStyle().getSelfRule().setProperties(FONT_STYLE);
        tabs.addTab(beanTable, attLbl);

        final WGraphicText styleSelfLbl = new WGraphicText(new Chars("\uE003"));
        styleSelfLbl.getStyle().getSelfRule().setProperties(FONT_STYLE);
        tabs.addTab(styleSelfRuleTable, styleSelfLbl);

        final WGraphicText styleLbl = new WGraphicText(new Chars("\uE004"));
        styleLbl.getStyle().getSelfRule().setProperties(FONT_STYLE);
        tabs.addTab(styleTable, styleLbl);

        final WSplitContainer splitTreeView = new WSplitContainer(false);
        final WSplitContainer splitTreeAtt = new WSplitContainer(true);
        final WSplitContainer splitViewPalette = new WSplitContainer(false);

        splitTreeAtt.addChild(widgetTree, SplitConstraint.TOP);
        splitTreeAtt.addChild(tabs, SplitConstraint.BOTTOM);

        splitTreeView.addChild(splitTreeAtt, SplitConstraint.LEFT);
        splitTreeView.addChild(previewPane, SplitConstraint.RIGHT);
        splitTreeView.setSplitPosition(250);

        splitViewPalette.addChild(splitTreeView, SplitConstraint.LEFT);
        splitViewPalette.addChild(paletteTable, SplitConstraint.RIGHT);
        splitViewPalette.setSplitPosition(800);

        addChild(toolbar, BorderConstraint.TOP);
        addChild(splitViewPalette, BorderConstraint.CENTER);

        toolbar.setLayout(new FormLayout());
        ((FormLayout)toolbar.getLayout()).setColumnSize(2, FormLayout.SIZE_EXPAND);

        final WMenuButton buttonOpen = new WMenuButton(new Chars("Open"),null, openListener);
        final WGraphicText openGraphic = new WGraphicText(new Chars("\ue017"));
        openGraphic.getStyle().getSelfRule().setProperties(FONT_STYLE);
        buttonOpen.setGraphic(openGraphic);
        toolbar.addChild(buttonOpen, FillConstraint.builder().coord(0, 0).build());
        final WMenuButton buttonSave = new WMenuButton(new Chars("Save"),null, saveListener);
        final WGraphicText saveGraphic = new WGraphicText(new Chars("\ue014"));
        saveGraphic.getStyle().getSelfRule().setProperties(FONT_STYLE);
        buttonSave.setGraphic(saveGraphic);
        toolbar.addChild(buttonSave, FillConstraint.builder().coord(1, 0).build());

        toolbar.addChild(new WLabel(new Chars("     Theme : ")), FillConstraint.builder().coord(3, 0).build());
        toolbar.addChild(createThemeButton(SystemStyle.THEME_LIGHT, new Chars(" Light ")), FillConstraint.builder().coord(4, 0).build());
        toolbar.addChild(createThemeButton(SystemStyle.THEME_DARK, new Chars(" Dark ")), FillConstraint.builder().coord(5, 0).build());

        final WContainer menu = new WContainer(new GridLayout(-1, 1));
        menu.addChild(new WMenuButton(new Chars("Open")), null);
        menu.addChild(new WMenuButton(new Chars("Save")), null);
        menu.addChild(new WSeparator(true), null);
        menu.addChild(new WMenuButton(new Chars("Exit")), null);
        buttonSave.setPopup(menu);


        final DefaultColumn paletteColumn = new DefaultColumn(new Chars("Palette"), new PalettePresenter());
        paletteColumn.setBestWidth(FormLayout.SIZE_EXPAND);
        paletteTable.getColumns().add(paletteColumn);


        widgetTree.setHeaderVisible(false);
        final DefaultTreeColumn treeCol = new DefaultTreeColumn(null,(Widget)null,new TreePresenter());
        treeCol.setBestWidth(FormLayout.SIZE_EXPAND);
        widgetTree.getColumns().add(treeCol);

        final DefaultColumn typeCol = new DefaultColumn((Widget)null,new TypePresenter());
        typeCol.setBestWidth(80);
        widgetTree.getColumns().add(typeCol);

        final DefaultColumn typeDelete = new DefaultColumn((Widget)null,new DeletePresenter());
        typeDelete.setBestWidth(20);
        widgetTree.getColumns().add(typeDelete);

        widgetTree.getSelection().addEventListener(CollectionMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                if(!widgetTree.getSelection().isEmpty()){
                    final Object candidate = widgetTree.getSelection().get(0);
                    setEdited(candidate);
                }
            }
        });

        widgetTree.addEventListener(new MessagePredicate(DragAndDropMessage.class), new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                final DragAndDropMessage evt = (DragAndDropMessage) event.getMessage();
                final Attachment attachment = (Attachment) evt.getBag().getAttachments().get(0);
                final Class widgetClass = (Class) attachment.getObject();

                try {
                    final Widget widget = (Widget) widgetClass.newInstance();
                    if(!widgetTree.getSelection().isEmpty()){
                        final Object candidate = widgetTree.getSelection().get(0);
                        if(candidate instanceof WidgetNode && ((WidgetNode)candidate).widget.getClass().equals(WContainer.class)){
                            ((WContainer)((WidgetNode)candidate).widget).addChild(widget, BorderConstraint.TOP);
                        }
                    }

                } catch (InstantiationException ex) {
                    ex.printStackTrace();
                } catch (IllegalAccessException ex) {
                    ex.printStackTrace();
                }
            }
        });

        previewPane.varRootWidget().sync(varRootWidget(), true);
        previewPane.varEdited().sync(varEdited());

        paletteTable.setRowModel(new DefaultRowModel(UIConstants.WIDGET_CLASSES));

        //sample root
        final WContainer root = new WContainer();
        root.setId(new Chars("parent"));
        root.setLayout(new BorderLayout());
        final WLabel label = new WLabel();
        label.setId(new Chars("label"));
        label.setText(new Chars("hello world"));
        label.setLayoutConstraint(BorderConstraint.CENTER);
        root.getChildren().add(label);
        setRootWidget(root);

    }

    public WContainer getRootWidget(){
        return (WContainer) getPropertyValue(PROPERTY_ROOTWIDGET);
    }

    public void setRootWidget(WContainer root){
        if(setPropertyValue(PROPERTY_ROOTWIDGET, root)){
            widgetTree.setRowModel(new TreeRowModel(new WidgetNode(root)));
        }
    }

    public Property varRootWidget(){
        return getProperty(PROPERTY_ROOTWIDGET);
    }

    public Object getEdited(){
        return (Object) getPropertyValue(PROPERTY_EDITED);
    }

    public void setEdited(Object edited) {
        if (edited instanceof WidgetNode) {
            edited = ((WidgetNode) edited).widget;
        } else if(edited instanceof LayoutNode){
            edited = ((LayoutNode) edited).layout;
        }

        if(setPropertyValue(PROPERTY_EDITED, edited)){
            beanTable.setBean((EventSource) edited);

            if(edited instanceof Widget){
                styleSelfRuleTable.setEdited((Widget) edited);
                styleTable.setEdited((Widget) edited);
            }else{
                styleSelfRuleTable.setEdited(null);
                styleTable.setEdited(null);
            }

            if(edited instanceof Layout){
                widgetTree.getSelection().replaceAll(new Object[]{new LayoutNode((Layout) edited)});
            }else if(edited instanceof Widget){
                widgetTree.getSelection().replaceAll(new Object[]{new WidgetNode((Widget) edited)});
            }else {
                widgetTree.getSelection().removeAll();
            }

        }
    }

    public Property varEdited(){
        return getProperty(PROPERTY_EDITED);
    }

    private static WMenuButton createThemeButton(final Chars path, Chars name){
        final WMenuButton button = new WMenuButton(name,null,new EventListener(){
            @Override
            public void receiveEvent(Event event) {
                final WStyle style = RSReader.readStyle(Paths.resolve(path));
                WidgetStyles.mergeDoc(
                        SystemStyle.INSTANCE.getRule(SystemStyle.RULE_SYSTEM),
                        style.getRule(SystemStyle.RULE_SYSTEM), false);
                SystemStyle.INSTANCE.notifyChanged();
            }
        });
        return button;
    }



    private static final class PalettePresenter implements ObjectPresenter{
        @Override
        public Widget createWidget(Object candidate) {
            return new PaletteWidgetView((Class)candidate);
        }
    }

    private static final class PaletteWidgetView extends WContainer{

        private final Class widgetClass;

        public PaletteWidgetView(Class widgetClass) {
            super(new BorderLayout());
            this.widgetClass = widgetClass;
            addChild(new WLabel(new Chars(widgetClass.getSimpleName())), BorderConstraint.CENTER);

            //start drag on mouse press
            addEventListener(MouseMessage.PREDICATE, new EventListener() {
                @Override
                public void receiveEvent(Event event) {
                    final MouseMessage me = (MouseMessage) event.getMessage();
                    if(me.getType()==MouseMessage.TYPE_PRESS){
                        final Attachment attachment = new DefaultAttachment(PaletteWidgetView.this.widgetClass, new Chars("widget.class"));
                        science.unlicense.system.System.get().getDragAndDrapBag().getAttachments().add(attachment);
                    }
                }
            });
        }

    }

    private static final class TreePresenter implements ObjectPresenter{
        @Override
        public Widget createWidget(Object candidate) {
            return new NodeWidget((Node)candidate);
        }
    }

    private static final class TypePresenter implements ObjectPresenter{
        @Override
        public Widget createWidget(Object candidate) {
            if(candidate instanceof WidgetNode){
                final WidgetNode node = (WidgetNode) candidate;
                candidate = node.widget;
            }
            final WLabel lbl = new WLabel();
            if(candidate instanceof Widget){
                lbl.setText(new Chars(candidate.getClass().getSimpleName()));
            }
            return lbl;
        }
    }

    private static final class DeletePresenter implements ObjectPresenter{
        @Override
        public Widget createWidget(Object candidate) {
            if(candidate instanceof WidgetNode && ((WidgetNode) candidate).widget.getParent()!=null){
                final WidgetNode node = (WidgetNode) candidate;
                final WMenuButton button = new WMenuButton(new Chars("X"), null, new EventListener() {
                    @Override
                    public void receiveEvent(Event event) {
                        if(node.widget.getParent() != null){
                            node.widget.getParent().getChildren().remove(node.widget);
                        }
                    }
                });
                return button;
            }else{
                return new WSpace();
            }
        }
    }

    private static final class NodeWidget extends WLabel{

        private final Node candidate;

        public NodeWidget(Node candidate) {
            this.candidate = candidate;
            if(this.candidate instanceof WidgetNode){
                final WidgetNode node = (WidgetNode) candidate;
                varText().sync(node.widget.varId());

            }else if(this.candidate instanceof LayoutNode){
                final LayoutNode node = (LayoutNode) candidate;
                setText(new Chars(node.layout.getClass().getSimpleName()));
            }
        }

    }

    private static final class WidgetNode extends DefaultNode {

        private final Widget widget;

        public WidgetNode(Widget widget) {
            super(widget.getClass().equals(WContainer.class));
            this.widget = widget;

            if(widget.getClass().equals(WContainer.class)){
                final EventListener lst = new EventListener() {
                    public void receiveEvent(Event event) {
                        updateNodes();
                    }
                };
                widget.getChildren().addEventListener(CollectionMessage.PREDICATE, lst);
                widget.addEventListener(new PropertyPredicate(WContainer.PROPERTY_LAYOUT), lst);
                updateNodes();
            }
        }

        private void updateNodes(){

            final Object[] subs = widget.getChildren().toArray();
            final Sequence nodes = new ArraySequence();
            nodes.add(new LayoutNode(((WContainer)widget).getLayout()));

            for(int i=0;i<subs.length;i++){
                int index = getChildren().search(subs[i]);
                if(index<0){
                    nodes.add(new WidgetNode((Widget) subs[i]));
                }else{
                    nodes.add(getChildren().get(index));
                }
            }
            getChildren().replaceAll(nodes);
        }

        public boolean equals(Object obj) {
            if(obj instanceof WidgetNode){
                return ((WidgetNode)obj).widget == widget;
            }else if(obj instanceof Widget){
                return obj == widget;
            }
            return false;
        }

    }

    private static final class LayoutNode extends DefaultNode {

        private final Layout layout;

        public LayoutNode(Layout layout) {
            super(false);
            this.layout = layout;
        }

        public boolean equals(Object obj) {
            if(obj instanceof LayoutNode){
                return ((LayoutNode)obj).layout == layout;
            }else if(obj instanceof Layout){
                return obj == layout;
            }
            return false;
        }
    }

}