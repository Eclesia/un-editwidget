
package science.unlicense.project.uieditor.adaptor;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.number.Int32;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.LayoutConstraint;
import science.unlicense.project.uieditor.UIConstants;

/**
 *
 * @author Johann Sorel
 */
public class LayoutContraintAdaptor implements ValueAdaptor {

    private static final Chars FORMCONSTRAINT_X = new Chars("x");
    private static final Chars FORMCONSTRAINT_Y = new Chars("y");
    private static final Chars FORMCONSTRAINT_SPANX = new Chars("spanx");
    private static final Chars FORMCONSTRAINT_SPANY = new Chars("spany");
    private static final Chars FORMCONSTRAINT_FILLX = new Chars("fillx");
    private static final Chars FORMCONSTRAINT_FILLY = new Chars("filly");

    @Override
    public Class getInputClass() {
        return LayoutConstraint.class;
    }

    @Override
    public Class getOutputClass() {
        return Object.class;
    }

    @Override
    public Object convert(Object value) {
        if(value == null) return null;

        //search for a static field with this value
        final Field[] fields = value.getClass().getFields();
        for(Field f : fields){
            if (Modifier.isStatic(f.getModifiers())) {
                try {
                    if(CObjects.equals(f.get(null),value)){
                        return new Chars(f.getDeclaringClass().getName()+"."+f.getName());
                    }
                } catch (IllegalArgumentException ex) {
                    //nothing
                } catch (IllegalAccessException ex) {
                    //nothing
                }
            }
        }

        if(value instanceof FillConstraint){
            final FillConstraint fct = (FillConstraint) value;
            final Dictionary dico = new HashDictionary();
            dico.add(UIConstants.ATT_CLASS,new Chars(FillConstraint.class.getName()));
            dico.add(FORMCONSTRAINT_X, fct.getX());
            dico.add(FORMCONSTRAINT_Y, fct.getY());
            dico.add(FORMCONSTRAINT_SPANX, fct.getSpanX());
            dico.add(FORMCONSTRAINT_SPANY, fct.getSpanY());
            dico.add(FORMCONSTRAINT_FILLX, fct.isFillX());
            dico.add(FORMCONSTRAINT_FILLY, fct.isFillY());
            return dico;
        }

        return null;
    }

    @Override
    public Object reverse(Object value) {
        if (value == null) return null;

        if (value instanceof Chars) {
            final Chars name = (Chars) value;
            final int idx = name.getLastOccurence('.');
            if (idx > 0) {
                final Chars className = name.truncate(0, idx);
                final Chars varName = name.truncate(idx+1, name.getCharLength());
                try {
                    final Class<?> clazz = Class.forName(className.toString());
                    final Field field = clazz.getField(varName.toString());
                    return field.get(null);
                } catch (Exception ex) {
                    //nothing
                }
            }
        } else if (value instanceof Dictionary) {
            final Dictionary dico = (Dictionary) value;
            final int x = Int32.decode((Chars) dico.getValue(FORMCONSTRAINT_X));
            final int y = Int32.decode((Chars) dico.getValue(FORMCONSTRAINT_Y));
            final int spanx = Int32.decode((Chars) dico.getValue(FORMCONSTRAINT_SPANX));
            final int spany = Int32.decode((Chars) dico.getValue(FORMCONSTRAINT_SPANY));
            final boolean fillx = (Boolean) dico.getValue(FORMCONSTRAINT_FILLX);
            final boolean filly = (Boolean) dico.getValue(FORMCONSTRAINT_FILLY);
            return FillConstraint.builder().coord(x, y).span(spanx, spany).build();
        }

        return value;
    }

}
