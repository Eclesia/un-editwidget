
package science.unlicense.project.uieditor.adaptor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;


/**
 *
 * @author Johann Sorel
 */
public class IntegerAdaptor extends AbstractValueAdaptor {

    public IntegerAdaptor() {
        super(Integer.class, Chars.class);
    }

    @Override
    public Object convert(Object value) {
        return Int32.encode((Integer) value);
    }

    @Override
    public Object reverse(Object value) {
        return Int32.decode((Chars) value);
    }

}
