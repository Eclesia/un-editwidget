
package science.unlicense.project.uieditor.adaptor;

/**
 *
 * @author Johann Sorel
 */
public interface ValueAdaptor {

    Class getInputClass();

    Class getOutputClass();

    Object convert(Object value);

    Object reverse(Object value);

}
