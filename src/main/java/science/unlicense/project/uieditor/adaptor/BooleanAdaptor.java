
package science.unlicense.project.uieditor.adaptor;

import science.unlicense.common.api.character.Chars;


/**
 *
 * @author Johann Sorel
 */
public class BooleanAdaptor extends AbstractValueAdaptor {

    private static final Chars TRUE = new Chars("true");
    private static final Chars FALSE = new Chars("false");

    public BooleanAdaptor() {
        super(Boolean.class, Chars.class);
    }

    @Override
    public Object convert(Object value) {
        return Boolean.TRUE.equals(value) ? TRUE : FALSE;
    }

    @Override
    public Object reverse(Object value) {
        return TRUE.equals(value, true, true);
    }

}
