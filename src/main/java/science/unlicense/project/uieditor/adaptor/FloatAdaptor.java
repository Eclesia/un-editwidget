
package science.unlicense.project.uieditor.adaptor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Float64;


/**
 *
 * @author Johann Sorel
 */
public class FloatAdaptor extends AbstractValueAdaptor {

    public FloatAdaptor() {
        super(Float.class, Chars.class);
    }

    @Override
    public Object convert(Object value) {
        return Float64.encode((Float) value);
    }

    @Override
    public Object reverse(Object value) {
        return (float)Float64.decode((Chars) value);
    }

}
