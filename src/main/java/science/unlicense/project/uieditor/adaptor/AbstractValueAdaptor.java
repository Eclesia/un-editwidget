
package science.unlicense.project.uieditor.adaptor;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractValueAdaptor implements ValueAdaptor {

    private final Class inputClass;
    private final Class outputClass;

    public AbstractValueAdaptor(Class inputClass, Class outputClass) {
        this.inputClass = inputClass;
        this.outputClass = outputClass;
    }

    @Override
    public Class getInputClass() {
        return inputClass;
    }

    @Override
    public Class getOutputClass() {
        return outputClass;
    }

}
