
package science.unlicense.project.uieditor.adaptor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Float64;


/**
 *
 * @author Johann Sorel
 */
public class DoubleAdaptor extends AbstractValueAdaptor {

    public DoubleAdaptor() {
        super(Double.class, Chars.class);
    }

    @Override
    public Object convert(Object value) {
        return Float64.encode((Double) value);
    }

    @Override
    public Object reverse(Object value) {
        return Float64.decode((Chars) value);
    }

}
