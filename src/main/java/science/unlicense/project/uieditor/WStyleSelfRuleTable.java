
package science.unlicense.project.uieditor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.engine.ui.io.RSReader;
import science.unlicense.engine.ui.io.RSWriter;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WTextArea;
import science.unlicense.engine.ui.widget.Widget;


/**
 *
 * @author Johann Sorel
 */
public class WStyleSelfRuleTable extends WContainer {

    public static final Chars PROPERTY_EDITED = new Chars("Edited");

    private final WTextArea textArea = new WTextArea();

    public WStyleSelfRuleTable(){
        super(new BorderLayout());
        addChild(textArea, BorderConstraint.CENTER);

        textArea.varText().addListener(new EventListener() {
            public void receiveEvent(Event event) {
                Widget edited = getEdited();
                if(edited !=null){
                    try{
                        edited.getStyle().getSelfRule().setProperties(textArea.getText().toChars());
                    }catch(Exception ex){
                        ex.printStackTrace();
                    }
                }
            }
        });

        textArea.setValidator(new Predicate() {
            public Boolean evaluate(Object candidate) {
                try{
                    RSReader.readStyle(new Chars("n : @rule {").concat((Chars)candidate).concat(new Chars("};")));
                    return true;
                }catch(Exception ex){
                    return false;
                }
            }
        });
    }

    public Widget getEdited(){
        return (Widget) getPropertyValue(PROPERTY_EDITED);
    }

    public void setEdited(Widget root){
        if(setPropertyValue(PROPERTY_EDITED, root)){
            if(root!=null){
                final Chars text = RSWriter.propertiesToChars(root.getStyle().getSelfRule());
                textArea.setText(text);
            }else{
                textArea.setText(Chars.EMPTY);
            }
        }
    }

}