
package science.unlicense.project.uieditor;

import static org.junit.Assert.*;
import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.io.RSWriter;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.project.uieditor.JSWidgetReader;
import science.unlicense.project.uieditor.JSWidgetWriter;

/**
 *
 * @author Johann Sorel
 */
public class JSIOReadWriteTest {

    /**
     * Test writing and reading a WLabel object.
     *
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    @Test
    public void testLabelReadWrite() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException{

        final WLabel lbl = new WLabel();
        lbl.setText(new Chars("test"));
        lbl.setEnable(false);
        lbl.setLayoutConstraint(BorderConstraint.RIGHT);

        //test writing
        final Chars txt = JSWidgetWriter.encode(lbl);
        assertEquals(new Chars(
                "{\n" +
                "  \"@class\":\"un.engine.ui.widget.WLabel\",\n" +
                "  \"Text\":\"test\",\n" +
                "  \"Enable\":\"false\",\n" +
                "  \"LayoutConstraint\":\"un.api.layout.BorderConstraint.RIGHT\",\n" +
                "  \"@style\":\"{}\"\n" +
                "}"),
                txt);

        //test reading
        final WLabel result = (WLabel) JSWidgetReader.decode(txt, null);
        assertEquals(lbl.getText(),result.getText());
        assertEquals(lbl.isEnable(),result.isEnable());
        assertEquals(lbl.getLayoutConstraint(),result.getLayoutConstraint());

    }

    /**
     * Test writing and reading a style.
     *
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    @Test
    public void testStyleReadWrite() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException{

        final WLabel lbl = new WLabel();
        lbl.getStyle().getSelfRule().setProperties(new Chars("margin:10"));
        lbl.getStyle().addRules(new Chars("{sub:{\nfilter:true\ncolor:rgba(255 0 255 255)\n}}"));

        //test writing
        final Chars txt = JSWidgetWriter.encode(lbl);
        assertEquals(new Chars(
                "{\n" +
                "  \"@class\":\"un.engine.ui.widget.WLabel\",\n" +
                "  \"@style\":\"{sub : {\nfilter : true\ncolor : rgba(255 0 255 255)\n}}\",\n" +
                "  \"@selfrule\":\"filter : true\nmargin : 10\"\n" +
                "}"),
                txt);

        //test reading
        final WLabel result = (WLabel) JSWidgetReader.decode(txt, null);
        assertEquals(RSWriter.propertiesToChars(lbl.getStyle().getSelfRule()), RSWriter.propertiesToChars(result.getStyle().getSelfRule()));
        assertEquals(lbl.getStyle().getRules().get(0), result.getStyle().getRules().get(0));

    }

    /**
     * Test writing and reading a WContainer object with BorderLayout
     *
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    @Test
    public void testLabelReadBorderLayoutContainer() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException{

        final WLabel lbl = new WLabel();
        lbl.setText(new Chars("test"));
        lbl.setEnable(false);
        lbl.setLayoutConstraint(BorderConstraint.TOP);

        final WContainer container = new WContainer(new BorderLayout());
        container.setId(new Chars("parent"));
        container.getChildren().add(lbl);

        //test writing
        final Chars txt = JSWidgetWriter.encode(container);
        assertEquals(new Chars(
                "{\n" +
                "  \"@class\":\"un.engine.ui.widget.WContainer\",\n" +
                "  \"Layout\":{\n" +
                "    \"@class\":\"un.api.layout.BorderLayout\"\n" +
                "  },\n" +
                "  \"Id\":\"parent\",\n" +
                "  \"children\":[\n" +
                "    {\n" +
                "      \"@class\":\"un.engine.ui.widget.WLabel\",\n" +
                "      \"Text\":\"test\",\n" +
                "      \"Enable\":\"false\",\n" +
                "      \"LayoutConstraint\":\"un.api.layout.BorderConstraint.TOP\",\n" +
                "      \"@style\":\"{}\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"@style\":\"{}\"\n" +
                "}"),
                txt);

        //test reading
        final WContainer resultCnt = (WContainer) JSWidgetReader.decode(txt, null);
        assertEquals(container.getId(),resultCnt.getId());
        assertTrue(resultCnt.getLayout() instanceof BorderLayout);
        assertEquals(1,resultCnt.getChildren().getSize());

        final WLabel resultLbl = (WLabel) resultCnt.getChildren().get(0);
        assertEquals(lbl.getText(),resultLbl.getText());
        assertEquals(lbl.isEnable(),resultLbl.isEnable());
        assertEquals(lbl.getLayoutConstraint(),resultLbl.getLayoutConstraint());
    }

    /**
     * Test writing and reading a WContainer object with FormLayout
     *
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    @Test
    public void testLabelReadFormLayoutContainer() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException{

        final WLabel lbl = new WLabel();
        lbl.setText(new Chars("test"));
        lbl.setEnable(false);
        lbl.setLayoutConstraint(FillConstraint.builder().coord(1, 2).span(3, 4).build());

        final WContainer container = new WContainer(new FormLayout());
        container.setId(new Chars("parent"));
        container.getChildren().add(lbl);

        //test writing
        final Chars txt = JSWidgetWriter.encode(container);
        assertEquals(new Chars(
                "{\n" +
                "  \"@class\":\"un.engine.ui.widget.WContainer\",\n" +
                "  \"Layout\":{\n" +
                "    \"@class\":\"un.api.layout.FormLayout\"\n" +
                "  },\n" +
                "  \"Id\":\"parent\",\n" +
                "  \"children\":[\n" +
                "    {\n" +
                "      \"@class\":\"un.engine.ui.widget.WLabel\",\n" +
                "      \"Text\":\"test\",\n" +
                "      \"Enable\":\"false\",\n" +
                "      \"LayoutConstraint\":{\n" +
                "        \"spany\":4,\n" +
                "        \"filly\":false,\n" +
                "        \"spanx\":3,\n" +
                "        \"fillx\":false,\n" +
                "        \"x\":1,\n" +
                "        \"y\":2,\n" +
                "        \"@class\":\"un.api.layout.FillConstraint\"\n" +
                "      },\n" +
                "      \"@style\":\"{}\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"@style\":\"{}\"\n" +
                "}"),
                txt);

        //test reading
        final WContainer resultCnt = (WContainer) JSWidgetReader.decode(txt, null);
        assertEquals(container.getId(),resultCnt.getId());
        assertTrue(resultCnt.getLayout() instanceof FormLayout);
        assertEquals(1,resultCnt.getChildren().getSize());

        final WLabel resultLbl = (WLabel) resultCnt.getChildren().get(0);
        assertEquals(lbl.getText(),resultLbl.getText());
        assertEquals(lbl.isEnable(),resultLbl.isEnable());
        assertEquals(lbl.getLayoutConstraint(),resultLbl.getLayoutConstraint());
    }


}
